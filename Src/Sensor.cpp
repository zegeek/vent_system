#include "Sensor.h"

Sensor::Sensor():Component()
{
}

Sensor::Sensor(SerialHandler *serial, std::string label):Component(serial, label)
{
    this->lastValue = this->get_value(serial);
    std::istringstream val_str(this->lastValue);
    val_str >> this->realValue;
}

void Sensor::display_info()
{
    std::cout << "Sensor "<< this->label<<" - value: "<< this->lastValue<<std::endl;
}
float Sensor::val(SerialHandler *serial)
{
    
    this->lastValue = this->get_value(serial);
    std::string val_str = this->lastValue;
    float val = 0;
    std::istringstream iss(val_str);
    iss >> val;
    return val;
}
Sensor::~Sensor()
{
    
}