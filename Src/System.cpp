#include "System.h"
System::System()
{
    this->target = NULL;
}
System::System(SerialHandler *target)
{
    std::cout << "Initiating system ---" << std::endl;
    this->target = target;
    std::cout << "Mapping sensors ---" <<std::endl;
    std::vector<SerialHandler::SensorTypedef> data = this->target->get_data();
    for(int i  = 0; i < data.size(); i++)
    {
        this->sensors[i] = Sensor(this->target,data.at(i).label);
    }
    std::cout << "Mapping actuators ---" <<std::endl;
    for(int i = 0; i< WRITE_SIZE; i++)
    {
        this->actuators[i] = Actuator(this->target,actuator_list[i].label,0);
    }
    this->state = true;
    std::cout << "System initiated." << std::endl;
}
System::~System()
{
    /*
    int j = 0;
    for(int i = 0;i< DATA_SIZE;i++)
    {
        delete (this->sensors+i);
        if(j < WRITE_SIZE)
        {
            delete (this->actuators+j);
            j++;
        }
    }
    */
}
bool System::is_up()
{
    return this->state;
}
float System::get_sensor_value(std::string label)
{
    bool found = false;
    int index = 0;
    float val = 0;
    while(index < DATA_SIZE && !found)
    {
        if(label == this->sensors[index].get_label())
            found = true;
        else
            index++;
    }
    //this->request_accessLock();
    val = this->sensors[index].val(this->target);
    //this->release_accessLock();
    return val;
}
void System::start_mode()
{
    if(this->state)
        this->target->set_mode_start();
}
void System::stop_mode()
{
    if(this->state)
        this->target->set_mode_stop();
}
void System::set_actuator_value(std::string label, float newValue)
{
    bool found = false;
    int index = 0;
    while(index < WRITE_SIZE && !found)
    {
        if(label == this->actuators[index].get_label())
            found = true;
        else
            index++;
    }
    //this->request_accessLock();
    this->actuators[index].val(this->target,newValue);
    //this->release_accessLock();
}
void System::display_info()
{
    std::cout<<"System current info: "<<std::endl;
    for(int i=0; i < DATA_SIZE;i++)
    {
        this->sensors[i].display_info();
    }
    for(int i=0; i < WRITE_SIZE;i++)
    {
        this->actuators[i].display_info();
    }
}