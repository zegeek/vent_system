#include "SerialHandler.h"

int SerialHandler::set_interface_attribs()
{
    struct termios tty;//structure to hold terminal device properties
    memset (&tty, 0, sizeof(tty));//reset structure value
    if(tcgetattr(this->fd, &tty) != 0)//getting attributes of opened device pointed by file descriptor fd
    {
        fprintf(stderr, "Error %d from tcgetattr", errno);
        return -1;
    }
    if(cfsetospeed(&tty, this->baudRate) != 0)
    {
        fprintf(stderr, "Error %d from cfsetospeed", errno);
        return -1;
    }
    if(cfsetispeed(&tty, this->baudRate) != 0)
    {
        fprintf(stderr, "Error %d from cfsetispeed", errno);
        return -1;
    }
    tty.c_cflag = (tty.c_cflag & ~CSIZE) | (this->wordLength);//set word length 8bit (one charachter at a time)
    tty.c_iflag &= ~IGNBRK;//disable break processing (\n)
    tty.c_lflag = 0; //disable canonical processing
    tty.c_oflag = 0;//disable output system treatement
    tty.c_oflag &=~OFILL;
    tty.c_cc[VMIN]  = this->blockingMode? 1 : 0;
    tty.c_cc[VTIME] = this->readTimeout;//read until one charachter or timeout
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);//disable output control
    tty.c_cflag |= (CLOCAL | CREAD);//enabling reading and disabling modem control
    tty.c_cflag &= ~(PARENB | PARODD);//disable parity control
    tty.c_cflag |= this->parity;
    tty.c_cflag &= ~CSTOPB;//disable stop bit
    cfmakeraw(&tty);
    tty.c_cflag &= ~CRTSCTS;//disable rts cts flow control
    if(tcsetattr(this->fd,TCSANOW,&tty) != 0)
    {
        fprintf(stderr, "Error %d from tcsetattr", errno);
        return -1;
    }
    return 0;
}
void SerialHandler::open_interface()
{
    if(!this->term_status)
    {
        this->fd = open(this->serialDevice.c_str(), O_RDWR | O_NOCTTY | O_SYNC);
        if(this-> fd > 0)
        {
            if(this->set_interface_attribs() == 0)
            {
                this->term_status = true;
                std::cout << "Port " << this->serialDevice << " opened succesfully." << std::endl;
            }
            else
            {
                std::cerr << "Error while setting attributes: " << std::endl;
            }
        }
        else
        {
            fprintf(stderr, "Error %d while opening %s: %s\n",errno,this->serialDevice.c_str(),strerror(errno));
        }
    }
    else
    {
        std::cerr << "Port " << this->serialDevice << " already opened." << std::endl;
    }
    
}
void SerialHandler::connect_target()
{
    std::string buffer = "";
    if(this->term_status && !this->connected)
    {
        
        do
        {
            this->uart_writestr(this->commands[ALIVE]);
            buffer = this->uart_readstr();
        }while(buffer.length() == 0);
        if(buffer == this->commands[ACK])
        {
            this->uart_writestr(this->commands[ACK]);
            this->connected = true;
        }
        else
            std::cerr << "Cannot connect to target. recieved msg: "<< buffer <<" "<< std::endl;
        
    }
    else
        std::cerr << "Cannot initiate connection to target. Either port is not initiated or the target is already connected."<< std::endl;
}
void SerialHandler::set_mode_start()
{
    
    this->uart_writestr(this->commands[START]);
    
}
void SerialHandler::set_mode_stop()
{
    this->uart_writestr(this->commands[STOP]);
}
void set_mode_stop();
std::string SerialHandler::get_sensor_value(const std::string label)
{
    std::string buffer = "";
    std::string value = "";
    if(this->connected)
    {
        
        do
        {
            this->uart_writestr(this->commands[ENTRY]);
            buffer = this->uart_readstr();
        }while(buffer.length() == 0);
        if(buffer == this->commands[ACK])
        {
            do
            {
                buffer = "ll"+label;
                this->uart_writestr(buffer);
                buffer.clear();
                buffer = this->uart_readstr();
            }while(buffer.length() == 0);
            if(buffer == label)
            {
                do
                {
                    this->uart_writestr(this->commands[ACK]);
                    buffer = this->uart_readstr();
                }while(buffer.length() == 0);
                value = buffer;
                do
                {
                    this->uart_writestr(this->commands[ACK]);
                    buffer = this->uart_readstr();
                }while(buffer.length() == 0);
                if(buffer == this->commands[ACK])
                {
                    this->uart_writestr(this->commands[ACK]);
                }
            }
        }
        
    }
    return value;
}
std::vector<SerialHandler::SensorTypedef> SerialHandler::get_data()
{
    std::string buffer = "";
    std::string label,value;
    std::vector<SerialHandler::SensorTypedef> dataArray;
    if(this->connected)
    {
        do
        {
            this->uart_writestr(this->commands[DATA]);
            buffer = this->uart_readstr();
        }while(buffer.length() == 0);
        while(buffer != this->commands[ACK])
        {
            label = buffer;
            buffer.clear();
            this->uart_writestr(this->commands[ACK]);
            buffer = this->uart_readstr();
            value = buffer;
            dataArray.push_back({label,value});
            buffer.clear();
            this->uart_writestr(this->commands[ACK]);
            buffer = this->uart_readstr();
        }
        this->uart_writestr(this->commands[ACK]);
    }
    return dataArray;
}
bool SerialHandler::update_actuator_value(SerialHandler::SensorTypedef actuator)
{
    std::string buffer = "";
    std::string label,value;
    if(this->connected)
    {
        do
        {
            this->uart_writestr(this->commands[WRITE]);
            buffer = this->uart_readstr();
        }while(buffer.length() == 0);
        label = "ll"+actuator.label;
        value = "vv"+actuator.value;
        if(buffer == this->commands[ACK])
        {
            buffer.clear();
            do
            {
                this->uart_writestr(label);
                buffer = this->uart_readstr();
            }while(buffer.length() == 0);
            if(buffer == this->commands[ACK])
            {
                do
                {
                    this->uart_writestr(value);
                    buffer = this->uart_readstr();
                }while(buffer.length() == 0);
                if(buffer == this->commands[ACK])
                {
                    this->uart_writestr(this->commands[ACK]);
                    return true;
                }
            }
        }
    }
    return false;
}
void SerialHandler::uart_writestr(const std::string str)
{
    char buffer[BUFFER_SIZE] = "";
    std::string buffer_str = "";
    if(this->term_status)
    {
        memset(buffer,0,BUFFER_SIZE);
        buffer_str = START_CHAR+str+END_CHAR;
        strcpy(buffer, buffer_str.c_str());
        if(write(this->fd,buffer,strlen(buffer)) < 0)
        {
            fprintf(stderr, "Error %d while writing %s to %s: %s", errno, buffer, this->serialDevice.c_str(), strerror(errno));
        }
    }
    else
        std::cerr << "Port "<< this->serialDevice <<" is not opened."<< std::endl;
}
std::string SerialHandler::uart_readstr()
{
    char cbuffer= 0;
    std::string buffer = "";
    if(this->term_status)
    {
        buffer.clear();
        do
        {
            read(this->fd,&cbuffer,1);
            if(cbuffer == START_CHAR)
            {
                buffer.clear();
            }
            else if(cbuffer == 1)
            {
                cbuffer = '#';
            }
            else if(cbuffer == 0 && buffer.length() == 0)
            {
                cbuffer = '$';
            }
            buffer.push_back(cbuffer);
        }while(cbuffer != END_CHAR && cbuffer != 0);
        buffer.erase(0,1);
        buffer.erase(buffer.length()-1,1);
        return buffer;
    }
    return buffer;
}

SerialHandler::SerialHandler()
{
}
SerialHandler::SerialHandler(std::string serialDevice, speed_t baudRate, int wordLength, bool blockingMode, int readTimeout, int parity)
{
    this->serialDevice = serialDevice;
    this->baudRate = baudRate;
    switch(wordLength)
    {
        case 8:
            this->wordLength = CS8;
            break;
        case 7:
            this->wordLength = CS7;
            break;
        case 6:
            this->wordLength = CS6;
            break;
        case 5:
            this->wordLength = CS5;
            break;
        default:
            this->wordLength = CS8;
            break;
    }
    this->blockingMode = blockingMode;
    this-> readTimeout = readTimeout;
    this->parity = parity;
    this->fd = -1;
    this->connected = false;
    this->term_status = false;
    this->open_interface();
}
bool SerialHandler::is_connected()
{
    return this->connected;
}

void SerialHandler::test()
{
    std::cout << "Begin Test alive" << std::endl;
    this->connect_target();
    if(this->connected)
        std::cout << "test succeeded." << std::endl;
    else
        std::cout << "test failed." << std::endl;
}
void SerialHandler::test_read_data()
{
    std::cout << "Begin test read data" << std::endl;
    std::vector<SensorTypedef> dataArray;
    if(this->connected)
    {
        dataArray = this->get_data();
        if(dataArray.size() > 0)
        {
            for(int i = 0; i < dataArray.size(); i++)
            {
                std::cout<<dataArray.at(i).label<<" - "<<dataArray.at(i).value<<std::endl;
            }
        }
        else
            std::cout << "no data retrieved" << std::endl;
    }
    else
        std::cout << "Test read data failed. maybe run test alive first." << std::endl;
}
void SerialHandler::test_read_write(const std::string label)
{
    std::cout << "Begin test read write data" << std::endl;
    std::string buffer = "";
    if(this->connected)
    {
        buffer = this->get_sensor_value(label);
        if(buffer.length() > 0)
            std::cout << label << " - "<< buffer<<std::endl;
        else
            std::cout << "No value for "<< label << std::endl;
        if(this->update_actuator_value({"EV_IN_AIR","9875"}))
            std::cout << "Updated EV_IN_AIR." << std::endl;
        else
            std::cout << "Error while update." << std::endl;
    }
    else
        std::cout << "Test read data failed. maybe run test alive first." << std::endl;
}
SerialHandler::~SerialHandler()
{
    if(this->term_status)
    {
        if(close(this->fd) != 0)
        {
            fprintf(stderr,"Error %d while closing %s: %s",errno, this->serialDevice.c_str(),strerror(errno));
        }
    }
}