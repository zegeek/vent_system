#include "Mode.h"

/*ModeCMV::ModeCMV()
{
    this->rate = 0;
    this->volume = 0.0;
    this->start_mode = false;
    this->breath = false;
}*/
ModeCMV::ModeCMV(DBHandler::SessionTypedef sess):rate(sess.session_rate),volume(sess.session_rate)
{
    this->rate = sess.session_rate;
    this->volume = sess.session_volume;
    this->oxygen_rate = sess.session_oxy_rate;
    this->pressure_threshold = sess.session_press_threshold;
    this->InsExpRation = sess.session_InsExpRate;
    this->ins_time = sess.session_ins_time;
    this->start_mode = false;
    this->start_emergency_count = false;
    this->breath = false;
}
/**
 * function member: start 
 * description: start simulation of CMV algorithm
 * */
void ModeCMV::start(System *sys)
{
    std::cout << "Starting CMV at "<< this->get_ins_period()<<"s breathing periode and "<<this->volume<<"cm3 "<<std::endl;
    this->start_mode = true;
    sys->start_mode();
    this->uptime = std::chrono::high_resolution_clock::now();
}
/**
 * function member: stop 
 * description: stop simulation of CMV algorithm
 * */
void ModeCMV::stop(System *sys)
{
    sys->stop_mode();
    this->stoptime = std::chrono::high_resolution_clock::now();
    this->start_mode = false;
}
/**
 * function member: is_started 
 * description: check if the algorithm as started or not
 * */
bool ModeCMV::is_started()
{
    return this->start_mode;
}
/**
 * function member: set_breath 
 * description: set the breath watcher variable to true
 * */
void ModeCMV::set_breath()
{
    if(!this->breathing_sequence)
    {
        this->stop_emergency_counting();
        this->breath = true;
    }
}
/**
 * function member: is_breathing 
 * description: check if there is a breath detected or not
 * */
bool ModeCMV::is_breathing()
{
    return this->breath;
}
/**
 * function member: unset_breath 
 * description: unset the breath watcher variable (false)
 * */
void ModeCMV::unset_breath()
{
    if(!this->breathing_sequence)
        this->breath = false;
}
/**
 * function member: set_breathing_seq 
 * description: set the breathing sequence watcher variable (true)
 * */
void ModeCMV::set_breathing_seq()
{
    this->breathing_sequence = true;
}
/**
 * function member: unset_breathing_seq 
 * description: unset the breathing sequence watcher variable (false)
 * */
void ModeCMV::unset_breathing_seq()
{
    this->breathing_sequence = false;
}
/**
 * function member: is_breathing_seq 
 * description: is the breathing sequence watcher true or not
 * */
bool ModeCMV::is_breathing_seq()
{
    return this->breathing_sequence;
}
/**
 * function member: start_emergency_counting
 * description: set the emergency counting watcher to true
 * */
void ModeCMV::start_emergency_counting()
{
    this->start_emergency_count = true;
}
/**
 * function member: stop_emergency_counting
 * description: set the emergency counting watcher to false
 * */
void ModeCMV::stop_emergency_counting()
{
    this->start_emergency_count = false;
}
/**
 * function member: is_emergency_counting
 * description: check if the emergency counting watcher is set or unset
 * */
bool ModeCMV::is_emergency_counting()
{
    return this->start_emergency_count;
}
/**
 * function member: get_oxygen_rate
 * description: return the value of oxygen rate
 * */
float ModeCMV::get_oxygen_rate()
{
    return this->oxygen_rate;
}
/**
 * function member: get_volume
 * description: return the value of volume of air to administrate
 * */
float ModeCMV::get_volume()
{
    return this->volume;
}
/**
 * function member: get_presure_threshold
 * description: return the value of volume of the pressure threshold
 * */
float ModeCMV::get_pressure_threshold()
{
    return this->pressure_threshold;
}
/**
 * function member: set_oxygen_rate
 * description: set the oxygen rate
 * */
void ModeCMV::set_oxygen_rate(float newOxygenRate)
{
    this->oxygen_rate = newOxygenRate;
}
/**
 * function member: set_volume
 * description: set the volume to administrate
 * */
void ModeCMV::set_volume(float newVolume)
{
    this->volume = newVolume;
}
/**
 * function member: set_pressure_threshold
 * description: set the pressure threshold
 * */
void ModeCMV::set_pressure_threshold(float newPresTh)
{
    this->pressure_threshold = newPresTh;
}
/**
 * function member: get_ins_period
 * description: get from the rate, the inspiration period in seconds 
 * */
float ModeCMV::get_ins_period()
{
    float period = 0;
    float cycle = this->get_cycle();
    int ins_ratio = 0;
    int exp_ratio = 0;
    if(this->InsExpRation.length() > 0)
    {
        std::istringstream ssi(this->InsExpRation.substr(0,this->InsExpRation.find("/")-1));
        ssi >> ins_ratio;
        std::istringstream ssi2(this->InsExpRation.substr(this->InsExpRation.find("/")+1,this->InsExpRation.length() -1));
        ssi2 >> exp_ratio;
        period = cycle / (float)(ins_ratio+exp_ratio);
    }
    else if(this->ins_time != 0)
    {
        period = this->ins_time;
    }
    return period;
}
float ModeCMV::get_exp_period()
{
    float period = 0;
    float cycle = this->get_cycle();
    float ins_time = this->get_ins_period();
    period = cycle - ins_time;
    return static_cast<float>(period);
}
float ModeCMV::get_cycle()
{
    return static_cast<float>(60.0 / this->rate);
}
/**
 * function member: error_rate
 * description: calculate the error rate of the value
 * */
float ModeCMV::error_rate(float value, float _error)
{
    return ((_error * value)/100);
}
/**
 * These functions are meant to work as independant threads in the main program, or the algoryhtm wrapper
 * They use the utilities above
 * */
/**
 * function member: param_watcher
 * description: this function check the P_OUT_EXP, C_IN_O2 and P_OUT_INS CONTINUOUSLY and controls CMV cycle
 * */
void ModeCMV::mode_CMV_core(System *sys, ModeCMV *mode)
{
    float p_out_exp = 0;
    float c_in_o2 = 0;
    float p_out_ins = 0;
    bool cycle = false;
    std::chrono::time_point<std::chrono::high_resolution_clock> start_breath;
    std::chrono::time_point<std::chrono::high_resolution_clock> end_breath;
    std::chrono::duration<float> duration;
    mode->start_emergency_counting();
    while(mode->is_started())
    { 
        p_out_exp =  sys->get_sensor_value("P_OUT_EXP");
        c_in_o2 = sys->get_sensor_value("C_IN_O2");
        p_out_ins = sys->get_sensor_value("P_OUT_INS");
        if(p_out_exp < 0)
        {
            mode->set_breath();
        }
        else
        {
            mode->unset_breath();
        }
        ///////////////
        if(c_in_o2 < (mode->get_oxygen_rate() - mode->error_rate(mode->get_oxygen_rate(),2.0)))
        {
            
            sys->set_actuator_value("EV_IN_O2",1);

        }    //more oxygen
        else if(c_in_o2 > (mode->get_oxygen_rate() + mode->error_rate(mode->get_oxygen_rate(),2.0)))
        {
            
            sys->set_actuator_value("EV_IN_O2",0);

        }
        ///////////////
        if(p_out_ins > (mode->get_pressure_threshold() - mode->error_rate(mode->get_pressure_threshold(),2.0)))
        {

            sys->set_actuator_value("EV_SEC",1);

        }
        else
        {

            sys->set_actuator_value("EV_SEC",0);

        }
        if(mode->is_breathing() && !mode->is_breathing_seq() || mode->is_emergency_counting())
        {
            start_breath = std::chrono::high_resolution_clock::now();
            mode->set_breathing_seq();
            cycle = true;
            if(mode->is_emergency_counting())
            {
                std::cout << "!!EMERGENCY!! ";
            }
            std::cout<< "is breathing" << std::endl;
            mode->engage_breathing_sequence(sys);
            mode->stop_emergency_counting();
        }
        else if(mode->is_breathing_seq())
        {
            end_breath = std::chrono::high_resolution_clock::now();
            duration = std::chrono::duration_cast<std::chrono::duration<float>>(end_breath - start_breath);
            if((duration.count() >= mode->get_ins_period() - mode->error_rate(mode->get_ins_period(),2.0)) && cycle)
            {
                cycle = false;
                std::cout << "end breathing after "<< duration.count() <<" seconds" << std::endl;
                mode->stop_breathing_sequence(sys);
            }
            if((duration.count() >= mode->get_cycle() - mode->error_rate(mode->get_cycle(),2.0)))
            {
                std::cout << "end cycle after "<< duration.count() <<" seconds" << std::endl;
                mode->unset_breathing_seq();
                mode->start_emergency_counting();
            }
        }
            
    }
}
/**
 * function: engage_breathing_sequence
 * description: This function open EV_OUT_INS and closes EV_OUT_EXP so that air can go to patient
 * */
void ModeCMV::engage_breathing_sequence(System *sys)
{
    sys->set_actuator_value("EV_OUT_EXP",0);
    sys->set_actuator_value("EV_OUT_INS",1);
}
/**
 * function: stop_breathing_sequence
 * description: This function closes EV_OUT_EXP and open EV_OUT_INS so that patient can expel air
 * */
void ModeCMV::stop_breathing_sequence(System *sys)
{
    sys->set_actuator_value("EV_OUT_EXP",1);
    sys->set_actuator_value("EV_OUT_INS",0);
}
void ModeCMV::display_duration()
{
    int time_span = std::chrono::duration_cast<std::chrono::seconds>(this->stoptime - this->uptime).count();
    std::cout << "Simulation lasted "<< time_span<<" seconds." <<std::endl;
}