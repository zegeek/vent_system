#include "Actuator.h"

Actuator::Actuator():Component()
{
    this->realValue = 0;
}

Actuator::Actuator(SerialHandler *serial, std::string label, float newValue):Component(serial,label)
{

    this->realValue = newValue;
    this->lastValue = std::to_string(static_cast<int>(this->realValue));
    Component::set_value(serial,this->lastValue);

}
Actuator::~Actuator()
{

}
void Actuator::display_info()
{
    std::cout << "Actuator "<< this->label<<" - value: "<< this->lastValue<<std::endl;
}

void Actuator::val(SerialHandler *serial, float newValue)
{

    this->realValue = newValue;
    this->lastValue = std::to_string(static_cast<int>(this->realValue));
    this->set_value(serial,this->lastValue);

}