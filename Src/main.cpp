#include <iostream>
#include <thread>
#include "VentSystem.h"

/**
 * File: main.cpp
 * Description: main process to be executed by the software
 * Author: Belha
 * */
using namespace std;

int main(int argc, char *argv[])
{
    DBHandler db("tcp://127.0.0.1:3306","root","root");
    SerialHandler serial("/dev/ttyUSB0",DFLT_BRATE,8,true,1,0);
    if(db.get_state())
        cout << "Database is up" << endl;
    else
        cout << "Database is down" << endl;
    const DBHandler::SessionTypedef sess = db.create_session(12,23.0,60,560,"1/4",0);
    ModeCMV cmv(sess);
    cout << sess.session_label << endl;
    db.add_session(sess);
    serial.connect_target();
    if(serial.is_connected())
    {
        System my_system(&serial);
        if(my_system.is_up())
        {
            my_system.display_info();
            cout << "breathing periode: "<<cmv.get_ins_period()<<endl;
            cout << "Starting simulation of breath control: " << endl;
            cmv.start(&my_system);
            //thread domain
            thread mode_CMV_core = thread(&ModeCMV::mode_CMV_core,&my_system,&cmv);
            //thread domain
            usleep(60 * 1000 * 1000);
            cmv.stop(&my_system);
            //thread join
            mode_CMV_core.join();
            //thread join
            cout << "Ending simulation of breath control. " << endl;
            cmv.display_duration();
        }
    }
    
    return 0;
}