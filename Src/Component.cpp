#include "Component.h"

bool Component::set_value(SerialHandler *serial, std::string value)
{
    bool result = false;
    SerialHandler::SensorTypedef actuator = {this->label,this->lastValue};
    serial->request_accessLock();
    result = serial->update_actuator_value(actuator);
    serial->release_accessLock();
    return result;
}
std::string Component::get_value(SerialHandler *serial)
{
    std::string result;
    serial->request_accessLock();
    result = serial->get_sensor_value(this->label);
    serial->release_accessLock();
    std::istringstream val_str(result);
    val_str >> this->realValue;
    return result;
}
std::string Component::get_label()
{
    return this->label;
}
std::string Component::get_lastValue()
{
    return this->lastValue;
}
void Component::display_info()
{

}
Component::Component(SerialHandler *serial, std::string label)
{
    this->label = label;
    this->lastValue = "";
    this->realValue = 0;
}
Component::Component()
{
    this->label = "";
    this->lastValue = "";
    this->realValue = 0;
}
Component::~Component()
{

}