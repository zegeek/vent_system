#include "DBHandler.h"

DBHandler::DBHandler()
{
    try
    {
        this->driver = get_driver_instance();
        this->con = this->driver->connect(URI,USERNAME,USERPWD);
        this->con->setSchema("vent_system");
        this->state = true;
    }
    catch(sql::SQLException &e)
    {
        std::cout << "ERR: " << e.what() << std::endl;
        std::cout << "(Error CODE: " << e.getErrorCode() << ")" << std::endl;
        std::cout << "StateSQL: " << e.getSQLState() << " )" << std::endl;
        this->state = false;
    }
}
DBHandler::DBHandler(const char* uri,const char* user, const char* mdp)
{
    try
    {
        this->driver = get_driver_instance();
        this->con = this->driver->connect(uri,user,mdp);
        this->con->setSchema("vent_system");
        this->state = true;
    }
    catch(sql::SQLException &e)
    {
        std::cout << "ERR: " << e.what() << std::endl;
        std::cout << "(Error CODE: " << e.getErrorCode() << ")" << std::endl;
        std::cout << "StateSQL: " << e.getSQLState() << " )" << std::endl;
        this->state = false;
    }
}
DBHandler::SessionTypedef DBHandler::create_session(uint8_t session_rate, float session_oxy_rate, float session_press_threshold, float session_volume, std::string session_InsExpRate, float session_ins_time)
{
    DBHandler::SessionTypedef sess;
    std::stringstream ss;
    std::time_t rawtime = std::time(nullptr);
    std::tm tm = *std::localtime(&rawtime);
    ss << std::put_time(&tm, "%Y%m%d%H%M%S");
    ss >> sess.session_label;
    sess.session_date = rawtime;
    sess.session_rate = session_rate;
    if(session_oxy_rate >= 21)
        sess.session_oxy_rate = session_oxy_rate;
    else
        sess.session_oxy_rate = 21.0;
    ///
    if(session_press_threshold > 0)
        sess.session_press_threshold = session_press_threshold;
    else
        sess.session_press_threshold = 0;
    ///
    if(session_volume > 0)
        sess.session_volume = session_volume;
    else
        sess.session_volume = 0;
    ///
    if(session_InsExpRate.length() > 0 && session_ins_time == 0)
        sess.session_InsExpRate = session_InsExpRate;
    else
        sess.session_InsExpRate = "Undefined";
    ///
    if(session_ins_time > 0)
        sess.session_ins_time = session_ins_time;
    else
        sess.session_ins_time = 0;
    return sess;
}
DBHandler::VolumeTypedef DBHandler::create_volume(float value, DBHandler::SessionTypedef *sess)
{
    DBHandler::VolumeTypedef vol;
    std::stringstream ss;
    std::time_t rawtime = std::time(nullptr);
    vol.value = value;
    vol.entry_tmp = rawtime;
    vol.session = sess;
    return vol;
}
int DBHandler::add_volume(const DBHandler::VolumeTypedef vol)
{
    int result = 0;
    if(this->state)
    {
        try
        {
           this->stmt = this->con->createStatement();
           std::stringstream ss;
           std::string query;
           std::string timestamp;
           query = "INSERT INTO volumes(value,entry_tmp,session_label) VALUES('";
           query += std::to_string(vol.value) + "','";
           ss << vol.entry_tmp;
           ss >> timestamp;
           query += timestamp;
           query += "','"+vol.session->session_label+ "');";
           result = this->stmt->executeUpdate(query);
        }
        catch(sql::SQLException &e)
        {
            std::cout << "ERR: " << e.what() << std::endl;
            std::cout << "(Error CODE: " << e.getErrorCode() << ")" << std::endl;
            std::cout << "(StateSQL: " << e.getSQLState() << " )" << std::endl;
            this->state = false;
        }
    }
    return result;
}
DBHandler::FlowTypedef DBHandler::create_flow(float value, DBHandler::SessionTypedef *sess)
{
    return (DBHandler::FlowTypedef)this->create_volume(value,sess);
}
int DBHandler::add_flow(const DBHandler::FlowTypedef flow)
{
    int result = 0;
    if(this->state)
    {
        try
        {
           this->stmt = this->con->createStatement();
           std::stringstream ss;
           std::string query;
           std::string timestamp;
           query = "INSERT INTO flows(value,entry_tmp,session_label) VALUES('";
           query += std::to_string(flow.value) + "','";
           ss << flow.entry_tmp;
           ss >> timestamp;
           query += timestamp;
           query += "','"+flow.session->session_label+ "');";
           result = this->stmt->executeUpdate(query);
        }
        catch(sql::SQLException &e)
        {
            std::cout << "ERR: " << e.what() << std::endl;
            std::cout << "(Error CODE: " << e.getErrorCode() << ")" << std::endl;
            std::cout << "(StateSQL: " << e.getSQLState() << " )" << std::endl;
            this->state = false;
        }
    }
    return result;
}
int DBHandler::add_session(const SessionTypedef sess)
{
    int result = 0;
    if(this->state)
    {
        try
        {
           this->stmt = this->con->createStatement();
           std::stringstream ss;
           std::string query;
           std::string date;
           query = "INSERT INTO sessions(session_label,session_date,session_rate,session_oxy_rate,session_press_threshold,session_volume,session_InsExpRate,session_ins_time) VALUES('";
           query += sess.session_label + "','";
           std::tm tm = *std::localtime(&sess.session_date);
           ss << std::put_time(&tm, "%F %T");
           ss >> date;
           query += date;
           ss >> date;
           query += " "+date+ "','";
           query += std::to_string(sess.session_rate)+"','"+std::to_string(sess.session_oxy_rate)+"','"+std::to_string(sess.session_press_threshold)+"','"+std::to_string(sess.session_volume)+"','"+sess.session_InsExpRate+"','"+std::to_string(sess.session_ins_time)+"');";
           result = this->stmt->executeUpdate(query);
        }
        catch(sql::SQLException &e)
        {
            std::cout << "ERR: " << e.what() << std::endl;
            std::cout << "(Error CODE: " << e.getErrorCode() << ")" << std::endl;
            std::cout << "(StateSQL: " << e.getSQLState() << " )" << std::endl;
            this->state = false;
        }
    }
    return result;
}
bool DBHandler::get_state()
{
    return this->state;
}