set(tools  /usr/bin)

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_SYSROOT /home/zegeek/rpi_rootfs/rpi_rootfs)
set(CMAKE_FIND_ROOT_PATH ${CMAKE_SYSROOT})
set(CMAKE_C_COMPILER ${tools}/arm-linux-gnueabihf-gcc-8)
set(CMAKE_CXX_COMPILER ${tools}/arm-linux-gnueabihf-g++-8)
set(CMAKE_AR ${tools}/arm-linux-gnueabihf-ar)
set(CMAKE_LD ${tools}/arm-linux-gnueabihf-ld)
add_definitions( "-marm" )
#-mfpu=vfp -mfloat-abi=hard 
#includes
set( RPI_INCLUDE_DIR "${RPI_INCLUDE_DIR} -isystem ${CMAKE_SYSROOT}/usr/include/arm-linux-gnueabihf")
set( RPI_INCLUDE_DIR "${RPI_INCLUDE_DIR} -isystem ${CMAKE_SYSROOT}/usr/include")
set( RPI_INCLUDE_DIR "${RPI_INCLUDE_DIR} -isystem ${CMAKE_SYSROOT}/usr/local/include")
#lib
set( RPI_LIBRARY_DIR "${RPI_LIBRARY_DIR} -Wl,-rpath-link,${CMAKE_SYSROOT}/usr/lib/arm-linux-gnueabihf")
set( RPI_LIBRARY_DIR "${RPI_LIBRARY_DIR} -Wl,-rpath-link,${CMAKE_SYSROOT}/lib/arm-linux-gnueabihf")
set(CMAKE_LIBRARY_PATH ${CMAKE_SYSROOT}/lib/arm-linux-gnueabihf;${CMAKE_SYSROOT}/usr/lib/arm-linux-gnueabihf)

#set(CMAKE_LIBRARY_PATH ${CMAKE_LIBRARY_PATH} ; ${CMAKE_SYSROOT}/usr/lib/arm-linux-gnueabihf)
set( CMAKE_CXX_FLAGS        "-std=c++11 ${CMAKE_CXX_FLAGS} ${RPI_INCLUDE_DIR}" CACHE STRING "" FORCE)
set( CMAKE_C_FLAGS          "${CMAKE_C_FLAGS} ${RPI_INCLUDE_DIR}" CACHE STRING "" FORCE)
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${RPI_LIBRARY_DIR}" CACHE STRING "" FORCE)
# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
## for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)


