#ifndef SENSOR_H
#define SENSOR_H

/**
 * Class: Sensor.h
 * Description: Definition for class Sensor
 * Auth: Belha
 * this class inherits from Component
 * 
 * */
#include <sstream>
#include "LockItem.h"
#include "Component.h"

class Sensor: public Component
{
public:
    Sensor();
    Sensor(SerialHandler *serial, std::string label);
    ~Sensor();
    float val(SerialHandler *serial);
    void display_info() override;
};

#endif