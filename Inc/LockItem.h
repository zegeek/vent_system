#ifndef LOCKITEM_H
#define LOCKITEM_H

#include <mutex>

/**
 * Class: LockItem.h
 * Description: Definition for class LockItem
 * Auth: Belha
 * This class gives tool for semaphor access to objects
 * 
 * */

class LockItem
{
public:
    //static std::mutex mtx;
    void request_accessLock();
    void release_accessLock();
    LockItem();
    ~LockItem();
};

#endif