#ifndef ACTUATOR_H
#define ACTUATOR_H

/**
 * Class: Actuator.h
 * Description: Definition for class Actuator
 * Auth: Belha
 * this class inherits from Component
 * 
 * */
#include "LockItem.h"
#include "Component.h"

class Actuator: public Component
{
public:
    Actuator();
    Actuator(SerialHandler *serial, std::string label, float newValue);
    ~Actuator();
    void val(SerialHandler *serial, float newValue);
    void display_info() override;

};

#endif