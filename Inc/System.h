#ifndef SYSTEM_H
#define SYSTEM_H

/**
 * Class: System.h
 * Description: Definition for class System
 * Auth: Belha
 * This class is a model for the whole vent system, contains all components and Serial handler
 * */
#include <vector>
#include "SerialHandler.h"
#include "Sensor.h"
#include "Actuator.h"
#include "LockItem.h"

#define DATA_SIZE 7
#define WRITE_SIZE 5

#define EV_IN_AIR 0
#define EV_IN_O2 1
#define EV_SEC 2
#define EV_OUT_INS 3
#define EV_OUT_EXP 4

class System: public LockItem
{
    SerialHandler *target;
    Sensor sensors[DATA_SIZE];
    Actuator actuators[WRITE_SIZE];
    bool state = false;
    SerialHandler::SensorTypedef actuator_list[WRITE_SIZE] = {
        {"EV_IN_AIR","0"},
		{"EV_IN_O2","0"},
		{"EV_SEC","0"},
		{"EV_OUT_INS","0"},
		{"EV_OUT_EXP","0"}
    };
public:
    System();
    System(SerialHandler *target);
    bool is_up();
    void start_mode();
    void stop_mode();
    float get_sensor_value(std::string label);
    void set_actuator_value(std::string label, float newValue);
    ~System();
    void display_info();
};

#endif