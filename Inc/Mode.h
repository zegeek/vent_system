#ifndef MODE_H
#define MODE_H

#include <sstream>
#include <unistd.h>
#include <chrono>
#include <math.h>
#include "System.h"
#include "LockItem.h"
#include "DBHandler.h"

class ModeCMV
{
private:
    std::chrono::time_point<std::chrono::high_resolution_clock> uptime;
    std::chrono::time_point<std::chrono::high_resolution_clock> stoptime;
    bool start_mode;
    bool breath;
    bool start_emergency_count;
    bool breathing_sequence = false;
    uint8_t rate;
    float oxygen_rate;
    float pressure_threshold;
    float volume;
    std::string InsExpRation;
    float ins_time;
public: 
    //ModeCMV();
    ModeCMV(DBHandler::SessionTypedef sess);
    float error_rate(float value, float _error);
    void start(System *sys);
    void stop(System *sys);
    bool is_started();
    void set_breath();
    void unset_breath();
    bool is_breathing();
    void set_breathing_seq();
    void unset_breathing_seq();
    bool is_breathing_seq();
    void start_emergency_counting();
    void stop_emergency_counting();
    bool is_emergency_counting();
    float get_oxygen_rate();
    float get_volume();
    float get_pressure_threshold();
    void set_oxygen_rate(float newOxygenRate);
    void set_volume(float newVolume);
    void set_pressure_threshold(float newPresTh);
    float get_ins_period();
    float get_exp_period();
    float get_cycle();
    static void mode_CMV_core(System *sys, ModeCMV *mode);
    void engage_breathing_sequence(System *sys);
    void stop_breathing_sequence(System *sys);
    void display_duration();
};

#endif