#ifndef SERIALHANDLER_H
#define SERIALHANDLER_H

/**
 * Class: SerialHandler.h
 * Description: Definition for class SerialHandler
 * Auth: Belha
 * @param baudRate: takes values: B0 - B50 - B75 - B110 - B134 - B150 - B200 - B300 - B600 - B1200 - B1800 - B2400 - B4800 - B9600 - B19200 - B38400 - B57600 - B115200 - B230400
 *        blockingMode: should or not release process until a charachter is recieved
 *        readTimeout: int * 10^(-1) second
 * */
//system include
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include "LockItem.h"

//defines
#define BUFFER_SIZE 20 //must be defined same in target system
#define DFLT_BRATE B115200
#define START_CHAR '$'
#define END_CHAR '#'
#define N_TO_DO -1
#define ACK 0
#define ALIVE 1
#define DATA 2
#define ENTRY 3
#define WRITE 4
#define START 5
#define STOP 6


class SerialHandler: public LockItem
{
private:
    std::string serialDevice;
    speed_t baudRate;
    int wordLength;
    bool blockingMode;
    int readTimeout;
    int parity;
    int fd;
    bool term_status;
    bool connected;
    const std::string commands[7] = {"ack","alive","data","entry","write","start","stop"};
    int set_interface_attribs();
    void uart_writestr(const std::string str);
    std::string uart_readstr();
public:
    typedef struct
    {
        std::string label;
        std::string value;
    
    }SensorTypedef;
    SerialHandler();
    SerialHandler(std::string serialDevice, speed_t baudRate, int wordLength, bool blockingMode, int readTimeout, int parity);
    void open_interface();
    void connect_target();
    bool is_connected();
    std::string get_sensor_value(const std::string label);
    std::vector<SensorTypedef> get_data();
    bool update_actuator_value(SensorTypedef actuator);
    void set_mode_start();
    void set_mode_stop();
//unit tests
    void test();
    void test_read_data();
    void test_read_write(const std::string label);
    ~SerialHandler();
};
 
#endif