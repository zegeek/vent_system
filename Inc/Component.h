#ifndef COMPONENT_H
#define COMPONENT_H

/**
 * Class: Component.h
 * Description: Definition for class Component
 * Auth: Belha
 * this an abstract class, requieres reimplementing when heriting this class
 * 
 * */
#include <iostream>
#include <sstream>
#include <string>
#include "SerialHandler.h"

class Component
{
protected:
    std::string label;
    std::string lastValue;
    float realValue;
    //this function is only to be used when reimplementing constructor for Sensor subclass
public:
    Component();
    Component(SerialHandler *serial, std::string label);
    bool set_value(SerialHandler *serial, std::string value);//this function is only to be used when reimplementing constructor for Actuator subclass
    std::string get_value(SerialHandler *serial);
    std::string get_label();
    virtual void display_info();
    std::string get_lastValue();
    ~Component();
};

#endif