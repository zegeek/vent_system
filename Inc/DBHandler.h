#ifndef DBHANDLER_H
#define DBHANDLER_H

#include <iostream>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <string>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

#define URI "tcp://127.0.0.1:3306"
#define USERNAME "root"
#define USERPWD "root"

class DBHandler 
{
private:
    sql::Driver* driver;
    sql::Connection* con;
    sql::Statement* stmt;
    sql::ResultSet* res;
    bool state = false;

public:
    typedef struct
    {
        std::string session_label;
        time_t session_date;
        uint8_t session_rate;
        float session_oxy_rate;
        float session_press_threshold;
        float session_volume;
        std::string session_InsExpRate;
        float session_ins_time;
    }SessionTypedef;
    typedef struct
    {
        float value;
        time_t entry_tmp;
        SessionTypedef* session;
    }VolumeTypedef;
    typedef VolumeTypedef FlowTypedef;
    DBHandler();
    DBHandler(const char* uri,const char* user, const char* mdp);
    /**
     * sessions
     * */
    SessionTypedef create_session(uint8_t session_rate, float session_oxy_rate, float session_press_threshold, float session_volume, std::string session_InsExpRate, float session_ins_time);
    int add_session(const SessionTypedef sess);
    /**
     * volumes
     * */
    VolumeTypedef create_volume(float value, DBHandler::SessionTypedef *sess);
    int add_volume(const VolumeTypedef vol);
    /**
     * flow
     * */
    FlowTypedef create_flow(float value, DBHandler::SessionTypedef *sess);
    int add_flow(const FlowTypedef flow);
    bool get_state();
};

#endif